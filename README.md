# Portal Aberto Deploy

## Deploy do projeto completo

Deploy do Portal Aberto do [Instituto de Psicologia da USP](https://www.usp.br/interpsi/).

Para executar o banco, front-end e back-end do Portal Aberto, é necessário [Docker](https://docs.docker.com/get-docker/) e [Docker Compose](https://docs.docker.com/compose/install/), além de garantir que as portas 80, 3000 e 27017 estejam disponíveis.

Além deste repositório, deve-se baixar os repositórios da aplicação [front-end](https://gitlab.com/portal-aberto/portal-aberto-frontend) e da aplicação [back-end](https://gitlab.com/portal-aberto/portal-aberto-api), de forma que os três repositórios fiquem lado a lado conforme visto a seguir:

![image](imgs/repositorios-terminal.png "text")

Após isso, deve-se acessar o repositório Portal Aberto Deploy e executar o comando `docker-compose up`.

![image](imgs/executando.png "text")

A partir de então, o front-end estará disponível na porta local 80, o back-end estará disponível na porta 3000 e o banco de dados, na porta 27017.

![image](imgs/aplicacao-rodando.png "text")

## Comandos úteis

* Para executar apenas o serviço back-end e o banco de dados, deve-se executar o comando `docker-compose run api`.

* Para executar os testes de unidade do serviço back-end, deve-se executar o comando `docker-compose run api bundle exec rspec`.
